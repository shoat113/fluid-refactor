package com.paic.arch.jmsbroker.impl;

import com.paic.arch.jmsbroker.MqBroker;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.Broker;
import org.apache.activemq.broker.BrokerService;
import org.slf4j.Logger;

import javax.jms.*;

import java.lang.IllegalStateException;

import static org.slf4j.LoggerFactory.getLogger;

public class ActivemqBroker implements MqBroker {
    private static final Logger LOG = getLogger(ActivemqBroker.class);
    private BrokerService brokerService;
    private String aBrokerUrl;

    @Override
    public void init(String aBrokerUrl) throws Exception {
        this.aBrokerUrl = aBrokerUrl;
        brokerService = new BrokerService();
        brokerService.setPersistent(false);
        brokerService.addConnector(aBrokerUrl);
    }

    @Override
    public void start() throws Exception {
        brokerService.start();
    }

    @Override
    public void stop() throws Exception {
        brokerService.stop();
        brokerService.waitUntilStopped();
    }

    @Override
    public Connection createConnection(String aBrokerUrl) throws JMSException {
        this.aBrokerUrl = aBrokerUrl;
        ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(aBrokerUrl);
        return connectionFactory.createConnection();
    }

    @Override
    public long getCount(String aDestinationName) {
        Broker regionBroker = brokerService.getRegionBroker();
        for (org.apache.activemq.broker.region.Destination destination : regionBroker.getDestinationMap().values()) {
            if (destination.getName().equals(aDestinationName)) {
                return destination.getDestinationStatistics().getMessages().getCount();
            }
        }
        throw new IllegalStateException(String.format("Destination %s does not exist on broker at %s", aDestinationName, aBrokerUrl));
    }
}
