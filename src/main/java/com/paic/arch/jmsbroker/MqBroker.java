package com.paic.arch.jmsbroker;

import javax.jms.Connection;
import javax.jms.JMSException;

public interface MqBroker {
    void init(String aBrokerUrl) throws Exception;
    void start() throws Exception;
    void stop() throws Exception;
    Connection createConnection(String aBrokerUrl) throws JMSException;
    long getCount(String aDestinationName);
}
